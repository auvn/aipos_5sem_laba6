package com.auvn.net_manual.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.auvn.net_manual.db.DBAdaptor;
import com.auvn.net_manual.db.records.Article;
import com.auvn.net_manual.db.records.Category;
import com.auvn.net_manual.properties.Paths;

@Controller
@RequestMapping("/category/{categoryId}/article/{articleId}")
@SessionAttributes({ "article" })
public class ArticleContentPage {

	private final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private DBAdaptor dbAdaptor;

	@RequestMapping(method = RequestMethod.GET)
	public String showArticleContent(@PathVariable Integer categoryId,
			@PathVariable Integer articleId, ModelMap modelMap) {
		logger.info("Building article content page view model ");
		Category category = new Category();
		Article article = new Article();
		category = dbAdaptor.getCategory(categoryId);
		article = dbAdaptor.getArticle(articleId);
		modelMap.addAttribute("articleCategory", category);
		modelMap.addAttribute("article", article);
		logger.info("Returning view for page: " + Paths.ARTICLE_CONTENT_PAGE);

		return Paths.ARTICLE_CONTENT_PAGE;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteArticle(@PathVariable Integer categoryId,
			@PathVariable Integer articleId, ModelMap modelMap) {
		logger.info("Deleting article with id:" + articleId);
		dbAdaptor.deleteArticle(articleId);
		return "redirect:/category/" + categoryId;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String showArticleEditPage(@PathVariable Integer categoryId,
			@PathVariable Integer articleId, ModelMap modelMap) {
		logger.info("Building edit page for article: " + articleId);
		Article article;
		if (articleId == 0) {
			article = new Article();
			article.setId(articleId);
			article.setCategoryId(categoryId);
			article.setContent("Empty");
			article.setTitle("Empty");

		} else
			article = dbAdaptor.getArticle(articleId);

		modelMap.addAttribute("article", article);

		return Paths.ARTICLE_EDIT_PAGE;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String saveChanges(@ModelAttribute Article article,
			BindingResult result, @PathVariable Integer categoryId,
			@PathVariable Integer articleId, ModelMap modelMap) {

		logger.info("Saving changes for article id: " + articleId);
		dbAdaptor.updateArticle(article);
		return "redirect:/category/" + categoryId;
	}

	public DBAdaptor getDbAdaptor() {
		return dbAdaptor;
	}

	public void setDbAdaptor(DBAdaptor dbAdaptor) {
		this.dbAdaptor = dbAdaptor;
	}

}
