package com.auvn.net_manual.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.auvn.net_manual.properties.Paths;
import com.auvn.net_manual.properties.Variables;
import com.auvn.net_manual.ui.ArticlesListBuilder;
import com.auvn.net_manual.ui.CategoriesTreeBuilder;

@Controller
public class MainPage {

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private CategoriesTreeBuilder treeBuilder;
	@Autowired
	private ArticlesListBuilder articlesListBuilder;

	public MainPage() {
		logger.info("create new instance of main page");
	}

	@RequestMapping(value = "*", method = RequestMethod.GET)
	public String redirectFromIncorrect() {
		logger.info("Redirecting to index.html");
		return "redirect:" + Paths.INDEX_PATH;
	}

	@RequestMapping(value = "/" + Paths.INDEX_HTML_PAGE, method = RequestMethod.GET)
	public String welcomePage(ModelMap modelMap) {

		logger.info("Building view model");
		treeBuilder.repaintTree();
		modelMap.addAttribute(Variables.ROOT_NODE, treeBuilder.getRootNode());
		logger.info("Return welcome page: " + Paths.MAIN_PAGE);
		return Paths.MAIN_PAGE;
	}

	public CategoriesTreeBuilder getTreeBuilder() {
		return treeBuilder;
	}

	public void setTreeBuilder(CategoriesTreeBuilder treeBuilder) {
		this.treeBuilder = treeBuilder;
	}

	public ArticlesListBuilder getArticlesListBuilder() {
		return articlesListBuilder;
	}

	public void setArticlesListBuilder(ArticlesListBuilder articlesListBuilder) {
		this.articlesListBuilder = articlesListBuilder;
	}

}
