package com.auvn.net_manual.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.auvn.net_manual.db.records.Category;
import com.auvn.net_manual.properties.Paths;
import com.auvn.net_manual.ui.ArticlesListBuilder;

@Controller
@RequestMapping("/category/{id}")
@SessionAttributes({ "articlesCategory", "category" })
public class CategoryContentPage {

	private final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private ArticlesListBuilder articlesListBuilder;

	@RequestMapping(method = RequestMethod.GET)
	public String showCategoryContent(@PathVariable Integer id,
			ModelMap modelMap) {
		logger.info("Building articles page view model ");
		Category articlesCategory = articlesListBuilder.getDbAdaptor()
				.getCategory(id);
		modelMap.addAttribute("articles",
				articlesListBuilder.getArticlesList(articlesCategory));
		modelMap.addAttribute("articlesCategory", articlesCategory);
		logger.info("Returning view page: " + Paths.ARTICLES_PAGE);
		return Paths.ARTICLES_PAGE;

	}

	@RequestMapping(value = "/addArticle", method = RequestMethod.GET)
	public String showNewArticlePage(@PathVariable("id") Integer categoryId) {
		return "redirect:/category/" + categoryId + "/article/0/edit";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteCategory(@PathVariable("id") Integer categoryId,
			ModelMap modelMap) {
		logger.info("Deleting category with id: " + categoryId);
		articlesListBuilder.getDbAdaptor().deleteCategory(categoryId);
		logger.info("Redirecting to index page");
		return "redirect:/index.html";
	}

	@RequestMapping(value = "/edit/{newCategoryId}", method = RequestMethod.GET)
	public String showEditCategoryPage(@PathVariable("id") Integer categoryId,
			@PathVariable Integer newCategoryId, ModelMap modelMap) {
		logger.info("Bulding category for modelMap");
		Category category;
		if (newCategoryId == 0) {
			category = new Category();
			category.setId(0);
			category.setName("New Category");
			category.setCategoryId(categoryId);
		} else
			category = articlesListBuilder.getDbAdaptor().getCategory(
					categoryId);
		modelMap.addAttribute("category", category);
		logger.info("Returning:" + Paths.CATEGORY_EDIT_PAGE);
		return Paths.CATEGORY_EDIT_PAGE;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String saveChanges(@ModelAttribute Category category,
			@PathVariable("id") Integer categoryId, ModelMap modelMap) {
		logger.info("Saving changes for content of category id: " + categoryId);
		articlesListBuilder.getDbAdaptor().updateCategory(category);
		return "redirect:/index.html";
	}

	public ArticlesListBuilder getArticlesListBuilder() {
		return articlesListBuilder;
	}

	public void setArticlesListBuilder(ArticlesListBuilder articlesListBuilder) {
		this.articlesListBuilder = articlesListBuilder;
	}

}
