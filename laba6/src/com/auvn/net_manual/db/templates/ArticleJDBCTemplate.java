package com.auvn.net_manual.db.templates;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.auvn.net_manual.db.daos.ArticleDAO;
import com.auvn.net_manual.db.mappers.ArticleMapper;
import com.auvn.net_manual.db.records.Article;
import com.auvn.net_manual.db.records.Category;

public class ArticleJDBCTemplate implements ArticleDAO {
	@SuppressWarnings("unused")
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	private final Log logger = LogFactory.getLog(getClass());

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void add(String content, String title, Integer categoryId) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ ADD_SQL_QUERY + " , " + content + " , " + title + " , "
				+ categoryId + "");
		jdbcTemplate.update(ADD_SQL_QUERY, content, title, categoryId);
	}

	public void delete(Integer id) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ DELETE_SQL_QUERY + " , " + id);
		jdbcTemplate.update(DELETE_SQL_QUERY, id);
	}

	public Article get(Integer id) {
		logger.info("executing jdbcTemplate queryForObject method with sql query: "
				+ GET_SQL_QUERY);
		Article article = jdbcTemplate.queryForObject(GET_SQL_QUERY,
				new Object[] { id }, new ArticleMapper());
		logger.info("returning article object");
		return article;
	}

	public List<Article> list() {
		logger.info("executing jdbcTemplate query method with sql query: "
				+ LIST_SQL_QUERY);
		List<Article> articles = jdbcTemplate.query(LIST_SQL_QUERY,
				new ArticleMapper());
		logger.info("returning articles object");
		return articles;
	}

	public List<Article> list(Category category) {
		logger.info("executing jdbcTemplate query method with arguments: "
				+ LST_ARTS_OF_CATGS_SQL_QUERY + " , " + category.getId() + "");
		List<Article> articles = jdbcTemplate.query(
				LST_ARTS_OF_CATGS_SQL_QUERY, new Object[] { category.getId() },
				new ArticleMapper());
		logger.info("returning articles object");
		return articles;
	}

	public void update(Integer id, String content, String title,
			Integer categoryId) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ UPDATE_SQL_QUERY + " , " + content + " , " + title + " , "
				+ categoryId + " , " + id);
		jdbcTemplate.update(UPDATE_SQL_QUERY, content, title, categoryId, id);
	}

}
