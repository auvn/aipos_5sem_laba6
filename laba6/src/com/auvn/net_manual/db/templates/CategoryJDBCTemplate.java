package com.auvn.net_manual.db.templates;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.auvn.net_manual.db.daos.CategoryDAO;
import com.auvn.net_manual.db.mappers.CategoryMapper;
import com.auvn.net_manual.db.records.Category;

public class CategoryJDBCTemplate implements CategoryDAO {
	@SuppressWarnings("unused")
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	private final Log logger = LogFactory.getLog(getClass());

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void add(String name, Integer categoryId) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ ADD_SQL_QUERY + " , " + name + " , " + categoryId + "");
		jdbcTemplate.update(ADD_SQL_QUERY, name, categoryId);
	}

	public void delete(Integer id) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ DELETE_SQL_QUERY + " , " + id);
		jdbcTemplate.update(DELETE_SQL_QUERY, id);
	}

	public Category get(Integer id) {
		logger.info("executing jdbcTemplate queryForObject method with arguments: "
				+ GET_SQL_QUERY + " , " + id);
		Category category = jdbcTemplate.queryForObject(GET_SQL_QUERY,
				new Object[] { id }, new CategoryMapper());
		logger.info("returning category object");
		return category;
	}

	public List<Category> list() {
		logger.info("executing jdbcTemplate query method with sql query: "
				+ LIST_SQL_QUERY);
		List<Category> categories = jdbcTemplate.query(LIST_SQL_QUERY,
				new CategoryMapper());
		logger.info("returning categories object");
		return categories;
	}

	public List<Category> list(Category category) {
		String query = SUBCTGS_SQL_QUERY;
		if (category.getId() == null)
			query = ROOTCTGS_SQL_QUERY;
		logger.info("executing jdbcTemplate query method with arguments: "
				+ query + " , " + category.getId());
		List<Category> categories = jdbcTemplate.query(query,
				new Object[] { category.getId() }, new CategoryMapper());
		logger.info("returning subcategories object");
		return categories;
	}

	public void update(Integer id, String name, Integer categoryId) {
		logger.info("executing jdbcTemplate update method with arguments: "
				+ UPDATE_SQL_QUERY + " , " + name + " , " + categoryId + " , "
				+ id);
		jdbcTemplate.update(UPDATE_SQL_QUERY, name, categoryId, id);
	}

}
