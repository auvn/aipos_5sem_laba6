package com.auvn.net_manual.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.auvn.net_manual.db.daos.ArticleDAO;
import com.auvn.net_manual.db.records.Article;

public class ArticleMapper implements RowMapper<Article> {
	@Override
	public Article mapRow(ResultSet resultSet, int arg1) throws SQLException {
		Article article = new Article();

		article.setId(resultSet.getInt(ArticleDAO.DB_TABLE_COLUMN_ID));
		article.setTitle(resultSet.getString(ArticleDAO.DB_TABLE_COLUMN_TITLE));
		article.setContent(resultSet
				.getString(ArticleDAO.DB_TABLE_COLUMN_CONTENT));
		article.setCategoryId(resultSet
				.getInt(ArticleDAO.DB_TABLE_COLUMN_CATEGORY_ID));
		return article;
	}
}
