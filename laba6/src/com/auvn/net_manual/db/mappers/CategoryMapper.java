package com.auvn.net_manual.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.auvn.net_manual.db.daos.CategoryDAO;
import com.auvn.net_manual.db.records.Category;

public class CategoryMapper implements RowMapper<Category> {
	@Override
	public Category mapRow(ResultSet resultSet, int arg1) throws SQLException {
		Category category = new Category();
		category.setId(resultSet.getInt(CategoryDAO.DB_TABLE_COLUMN_ID));
		category.setName(resultSet.getString(CategoryDAO.DB_TABLE_COLUMN_NAME));
		category.setCategoryId(resultSet
				.getInt(CategoryDAO.DB_TABLE_COLUMN_CATEGORY_ID));
		return category;
	}
}
