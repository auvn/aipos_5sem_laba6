package com.auvn.net_manual.db;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.auvn.net_manual.db.records.Article;
import com.auvn.net_manual.db.records.Category;
import com.auvn.net_manual.db.templates.ArticleJDBCTemplate;
import com.auvn.net_manual.db.templates.CategoryJDBCTemplate;

public class DBAdaptor {
	@Autowired
	private CategoryJDBCTemplate categoryJDBCTemplate;
	@Autowired
	private ArticleJDBCTemplate articleJDBCTemplate;

	private final Log logger = LogFactory.getLog(getClass());

	public DBAdaptor() {
		logger.info("creating DBAdaptor object");
	}

	public List<Category> getRootCategories() {
		return categoryJDBCTemplate.list();
	}

	public List<Category> getCategories(Category category) {
		return categoryJDBCTemplate.list(category);
	}

	public Category getCategory(Integer id) {
		return categoryJDBCTemplate.get(id);
	}

	public Article getArticle(Integer id) {
		return articleJDBCTemplate.get(id);
	}

	public List<Article> getArticles(Category category) {
		return articleJDBCTemplate.list(category);
	}

	public void deleteCategory(Integer id) {
		categoryJDBCTemplate.delete(id);
	}

	public void deleteArticle(Integer id) {
		articleJDBCTemplate.delete(id);
	}

	public void updateArticle(Article article) {
		if (article.getId() == 0)
			articleJDBCTemplate.add(article.getContent(), article.getTitle(),
					article.getCategoryId());
		else
			articleJDBCTemplate.update(article.getId(), article.getContent(),
					article.getTitle(), article.getCategoryId());
	}

	public void updateCategory(Category category) {
		if (category.getId() == 0)
			categoryJDBCTemplate.add(category.getName(),
					category.getCategoryId());
		else
			categoryJDBCTemplate.update(category.getId(), category.getName(),
					category.getCategoryId());
	}

	public CategoryJDBCTemplate getCategoryJDBCTemplate() {
		return categoryJDBCTemplate;
	}

	public void setCategoryJDBCTemplate(
			CategoryJDBCTemplate categoryJDBCTemplate) {
		this.categoryJDBCTemplate = categoryJDBCTemplate;
	}

	public ArticleJDBCTemplate getArticleJDBCTemplate() {
		return articleJDBCTemplate;
	}

	public void setArticleJDBCTemplate(ArticleJDBCTemplate articleJDBCTemplate) {
		this.articleJDBCTemplate = articleJDBCTemplate;
	}

}
