package com.auvn.net_manual.db.daos;

import java.util.List;

import javax.sql.DataSource;

import com.auvn.net_manual.db.records.Article;
import com.auvn.net_manual.db.records.Category;

public interface ArticleDAO {
	public static final String DB_TABLE = "article";

	public static final String DB_TABLE_COLUMN_ID = "id";
	public static final String DB_TABLE_COLUMN_CONTENT = "content";
	public static final String DB_TABLE_COLUMN_TITLE = "title";
	public static final String DB_TABLE_COLUMN_CATEGORY_ID = "category_id";

	public static final String ADD_SQL_QUERY = "insert into " + DB_TABLE + " ("
			+ DB_TABLE_COLUMN_CONTENT + "," + DB_TABLE_COLUMN_TITLE + ","
			+ DB_TABLE_COLUMN_CATEGORY_ID + ") values (?,?,?)";

	public static final String DELETE_SQL_QUERY = "delete from " + DB_TABLE
			+ " where " + DB_TABLE_COLUMN_ID + "=?";

	public static final String GET_SQL_QUERY = "select * from " + DB_TABLE
			+ " where " + DB_TABLE_COLUMN_ID + "=?";

	public static final String LIST_SQL_QUERY = "select * from " + DB_TABLE;
	public static final String LST_ARTS_OF_CATGS_SQL_QUERY = "select * from "
			+ DB_TABLE + " where " + DB_TABLE_COLUMN_CATEGORY_ID + "=?";

	public static final String UPDATE_SQL_QUERY = "update " + DB_TABLE
			+ " set " + DB_TABLE_COLUMN_CONTENT + "=?," + DB_TABLE_COLUMN_TITLE
			+ "=?," + DB_TABLE_COLUMN_CATEGORY_ID + "=? where "
			+ DB_TABLE_COLUMN_ID + "=?";

	public void setDataSource(DataSource dataSource);

	public void add(String content, String title, Integer categoryId);

	public void delete(Integer id);

	public Article get(Integer id);

	public List<Article> list(Category category);

	public List<Article> list();

	public void update(Integer id, String content, String title,
			Integer categoryId);
}
