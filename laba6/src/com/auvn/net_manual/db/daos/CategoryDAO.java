package com.auvn.net_manual.db.daos;

import java.util.List;

import javax.sql.DataSource;

import com.auvn.net_manual.db.records.Category;

public interface CategoryDAO {
	public static final String DB_TABLE = "category";

	public static final String DB_TABLE_COLUMN_ID = "id";
	public static final String DB_TABLE_COLUMN_NAME = "name";
	public static final String DB_TABLE_COLUMN_CATEGORY_ID = "category_id";

	public static final String ADD_SQL_QUERY = "insert into " + DB_TABLE
			+ " (name,category_id) values (?,?)";
	public static final String DELETE_SQL_QUERY = "delete from " + DB_TABLE
			+ " where id=?";
	public static final String GET_SQL_QUERY = "select * from " + DB_TABLE
			+ " where id=?";
	public static final String LIST_SQL_QUERY = "select * from " + DB_TABLE;
	public static final String ROOTCTGS_SQL_QUERY = "select * from " + DB_TABLE
			+ " where " + DB_TABLE_COLUMN_CATEGORY_ID + " is ?";
	public static final String SUBCTGS_SQL_QUERY = "select * from " + DB_TABLE
			+ " where " + DB_TABLE_COLUMN_CATEGORY_ID + " =?";
	public static final String UPDATE_SQL_QUERY = "update " + DB_TABLE
			+ " set name=?,category_id=? where id=?";

	public void setDataSource(DataSource dataSource);

	public void add(String name, Integer categoryId);

	public void delete(Integer id);

	public Category get(Integer id);

	public List<Category> list();

	public List<Category> list(Category category);

	public void update(Integer id, String name, Integer categoryId);

}
