package com.auvn.net_manual.properties;

public interface Paths {
	public static final String MAIN_PAGE = "main_page";
	public static final String ARTICLES_PAGE = "category_content_page";
	public static final String ARTICLE_CONTENT_PAGE = "article_content_page";
	public static final String ARTICLE_EDIT_PAGE = "article_edit_page";
	public static final String CATEGORY_EDIT_PAGE = "category_edit_page";
	public static final String INDEX_PATH = "/index.html";
	public static final String INDEX_HTML_PAGE = "index.html";

}
