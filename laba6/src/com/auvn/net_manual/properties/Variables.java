package com.auvn.net_manual.properties;

public interface Variables {
	public static final String ROOT_NODE = "rootNode";
	public static final String SUB_NODE = "subNode";
	public static final String MY_TYPE_TREE_NODE = "my_type";
	public static final String CATEGORY_TREE_NODE = "categoryNodeType";
	public static final String ARTICLE_TREE_NODE = "articleNodeType";
}
