package com.auvn.net_manual.ui;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.auvn.net_manual.db.DBAdaptor;
import com.auvn.net_manual.db.records.Article;
import com.auvn.net_manual.db.records.Category;

public class ArticlesListBuilder {
	private final Log logger = LogFactory.getLog(getClass());

	private DBAdaptor dbAdaptor;

	public ArticlesListBuilder() {
		logger.info("Creating instance of articles list builder");
	}

	public List<Article> getArticlesList(Category category) {
		logger.info("Executing getArticles method of DBAdaptor");
		return dbAdaptor.getArticles(category);
	}

	public DBAdaptor getDbAdaptor() {
		return dbAdaptor;
	}

	public void setDbAdaptor(DBAdaptor dbAdaptor) {
		this.dbAdaptor = dbAdaptor;
	}

}
