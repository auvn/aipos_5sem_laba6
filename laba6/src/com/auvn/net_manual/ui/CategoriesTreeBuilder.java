package com.auvn.net_manual.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.auvn.net_manual.db.DBAdaptor;
import com.auvn.net_manual.db.records.Category;
import com.einnovates.customtags.tree.TreeNode;

public class CategoriesTreeBuilder {
	private final Log logger = LogFactory.getLog(getClass());
	private DBAdaptor dbAdaptor;
	private TreeNode rootNode;

	public CategoriesTreeBuilder() {
		logger.info("Creating TreeBuilder object");
	}

	public void repaintTree() {
		logger.info("Rebuilding manual's structure tree");
		rootNode = new TreeNode("root");
		Category rootCategory = new Category();
		rootCategory.setId(null);
		fillTree(rootNode, rootCategory);
	}

	private void fillTree(TreeNode currentTreeNode, Category currentCategory) {
		for (Category category : dbAdaptor.getCategories(currentCategory)) {
			TreeNode nextTreeNode = new TreeNode(category);
			fillTree(nextTreeNode, category);
			currentTreeNode.add(nextTreeNode);
		}
	}

	public TreeNode getRootNode() {
		logger.info("Returning rootNode of Tree");
		return rootNode;
	}

	public DBAdaptor getDbAdaptor() {
		return dbAdaptor;
	}

	public void setDbAdaptor(DBAdaptor dbAdaptor) {
		this.dbAdaptor = dbAdaptor;
	}

}
