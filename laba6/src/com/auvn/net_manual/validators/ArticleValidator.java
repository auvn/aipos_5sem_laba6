package com.auvn.net_manual.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.auvn.net_manual.db.records.Article;

public class ArticleValidator implements Validator {
	public boolean supports(Class<?> clazz) {
		return Article.class.isAssignableFrom(clazz);

	}

	public void validate(Object obj, Errors errors) {
		Article article = (Article) obj;
		errors.rejectValue("title", "OLOLOLO");
		errors.rejectValue("content", "OLOLOLO222");
	}
}
