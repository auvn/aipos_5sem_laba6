SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE SCHEMA IF NOT EXISTS `aipos_laba6` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci ;
USE `aipos_laba6` ;

-- —---------------------------------------------------
-- Table `aipos_laba6`.`category`
-- —---------------------------------------------------
CREATE TABLE IF NOT EXISTS `aipos_laba6`.`category` (
`id` INT NOT NULL AUTO_INCREMENT ,
`name` VARCHAR(45) NOT NULL ,
`category_id` INT NULL  ,
PRIMARY KEY (`id`) ,
INDEX `fk_category_category1_idx` (`category_id` ASC) ,
CONSTRAINT `fk_category_category1`
FOREIGN KEY (`category_id` )
REFERENCES `aipos_laba6`.`category` (`id` )
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB;
INSERT INTO `aipos_laba6`.`category` (`name`) values ('root');

-- —---------------------------------------------------
-- Table `aipos_laba6`.`article`
-- —---------------------------------------------------
CREATE TABLE IF NOT EXISTS `aipos_laba6`.`article` (
`id` INT NOT NULL AUTO_INCREMENT ,
`content` TEXT NULL ,
`title` VARCHAR(45) NOT NULL ,
`category_id` INT NOT NULL ,
PRIMARY KEY (`id`) ,
INDEX `fk_article_category_idx` (`category_id` ASC) ,
CONSTRAINT `fk_article_category`
FOREIGN KEY (`category_id` )
REFERENCES `aipos_laba6`.`category` (`id` )
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;