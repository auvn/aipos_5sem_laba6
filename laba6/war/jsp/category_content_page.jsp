<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${articlesCategory.name}</title>
</head>
<body>
	<core:out value="${articlesCategory.name}:" />
	<br>
	<div style="overflow: auto; float: left;">

		<table>
			<tr>

				<td><form:form method="GET"
						action="${articlesCategory.id}/addArticle">
						<input type="submit" align="left" value="Add Article" />
					</form:form></td>
				<td><form:form method="GET"
						action="${articlesCategory.id}/edit/${articlesCategory.id}">
						<input type="submit" align="left" value="Edit Category" />
					</form:form></td>
				<td><form:form method="GET"
						action="${articlesCategory.id}/edit/0">
						<input type="submit" align="left" value="Add Category" />
					</form:form></td>
				<td><a href='<core:url value="/index.html"/>'>Home</a></td>
			</tr>
		</table>

	</div>
	<br>
	<br>
	<core:out value="Articles:" />
	<br>
	<core:forEach items="${articles}" var="article">
		<a href="${articlesCategory.id}/article/${article.id}"><core:out
				value="${article.title}" /></a>
		<br>
	</core:forEach>
</body>
</html>