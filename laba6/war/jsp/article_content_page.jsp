<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${articleCategory.name}:${article.title}</title>
</head>
<body>

	<div style="overflow: auto; float: left;">
		<table>
			<tr>
				<td><form:form method="GET" action="${article.id}/edit">
						<input type="submit" align="left" value="Edit" />
					</form:form></td>
				<td><form:form method="DELETE" action="${article.id}/delete">
						<input type="submit" align="left" value="Delete" />
					</form:form></td>

				<td><a href='<core:url value="/index.html"/>'>Home</a></td>
			</tr>
		</table>
		<br>
		<core:out value="Article title: ${article.title}" />
		<br>
		<core:out value="Article content:" />
		<br>
		<form:textarea path="article.content" disabled="true" rows="50"
			cols="100" />
	</div>


</body>
</html>