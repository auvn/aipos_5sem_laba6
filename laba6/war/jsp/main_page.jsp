<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Network Technologies Manual</title>
</head>
<body id="main_page_body">

	<div style="overflow: auto; float: left;">
		<tree4jsp:tree treeData="${rootNode}" nodeObject="subNode"
			hideRoot="true" imageBase='images/tree/'>
			<tree4jsp:renderer nodeType="ALL">
				<a href="category/${subNode.id}">${subNode.name}</a>
			</tree4jsp:renderer>
			<!-- 	<tree4jsp:renderer nodeType="ROOT">
				[${subNode}]
			</tree4jsp:renderer> -->
		</tree4jsp:tree>
	</div>
</body>
</html>