<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${article.title}</title>
</head>
<body>
	<div style="overflow: auto; float: left;">
		<form:form method="POST" commandName="article">
			<table>
				<tr>
					<td><form:input path="title" /></td>
					<td><form:errors path="title" /></td>
				</tr>
				<tr>
					<td><form:textarea path="content" rows="50" cols="100" /></td>
				</tr>
				<tr>
					<td><input type="submit" align="center" value="Save" /></td>
					<td><a href='<core:url value="/index.html"/>'>Home</a></td>
				</tr>
			</table>
		</form:form>
	</div>

</body>
</html>