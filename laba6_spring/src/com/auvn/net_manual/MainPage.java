package com.auvn.net_manual;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.auvn.net_manual.properties.Paths;
import com.auvn.net_manual.properties.Variables;

@Controller
public class MainPage {

	private final Log logger = LogFactory.getLog(getClass());

	private TreeBuilder treeBuilder;

	public MainPage() {
		this.treeBuilder = new TreeBuilder();
	}

	@RequestMapping(value = "*", method = RequestMethod.GET)
	public String redirectFromIncorrect() {
		logger.info("Redirecting to index.html");
		return "redirect:" + Paths.INDEX_PATH;
	}

//	@RequestMapping(value = "{}", method = RequestMethod.GET)
//	public String getImage() {
//		return new ModelAndView("image", "test");
//	}

	
	
	@RequestMapping(value = "/" + Paths.INDEX_HTML_PAGE, method = RequestMethod.GET)
	public ModelAndView welcomePage() {

		logger.info("Building view model");
		ModelAndView modelAndView = new ModelAndView(Paths.MAIN_PAGE);
		treeBuilder.repaintTree();
		modelAndView.addObject(Variables.ROOT_NODE, treeBuilder.getRootNode());

		logger.info("Return welcome page");
		return modelAndView;
	}

	// @RequestMapping("")

}
