package com.auvn.net_manual.properties;

public interface Paths {
	public static final String MAIN_PAGE = "main_page";
	public static final String INDEX_PATH = "/manual/index.html";
	public static final String INDEX_HTML_PAGE = "index.html";

}
