package com.auvn.net_manual;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.auvn.net_manual.properties.Variables;
import com.einnovates.customtags.tree.TreeNode;

public class TreeBuilder {
	private final Log logger = LogFactory.getLog(getClass());

	private TreeNode rootNode;

	public void repaintTree() {
		logger.info("Rebuilding manual's structure tree");
		rootNode = new TreeNode("root");
		TreeNode subTreeNode = new TreeNode("test1");
		subTreeNode.add(new TreeNode("test11", Variables.ARTICLE_TREE_NODE));
		rootNode.add(subTreeNode);
		rootNode.add(new TreeNode("test2", Variables.ARTICLE_TREE_NODE));
	}

	public TreeNode getRootNode() {
		logger.info("Return rootNode of Tree");
		return rootNode;
	}

}
