<%@page import="com.einnovates.customtags.tree.TreeNode"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/jsp/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Network Technologies Manual</title>
</head>
<body>
	Page for application
	

<tree4jsp:tree treeData="${rootNode}" nodeObject="subNode" hideRoot="true" imageBase="/images/tree/">
	<tree4jsp:renderer nodeType="articleNodeType" > 
		<img class="leafImg" src='<core:url value = "/images/tree/doc.png" />' />  
		<a href="${subNode}" >${subNode}</a>
	</tree4jsp:renderer>
</tree4jsp:tree>
	
</body>
</html>